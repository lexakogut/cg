﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CG_App.Tools;

namespace CG_App.Test
{
	[TestClass]
	public class MatrixTests
	{
		void AssertMatrixEquality(Matrix matrix1, Matrix matrix2)
		{
			Assert.AreEqual(matrix1.Width, matrix2.Width);
			Assert.AreEqual(matrix1.Height, matrix2.Height);

			for (int y = 0; y < matrix1.Height; ++y)
			{
				for (int x = 0; x < matrix1.Width; ++x)
				{
					Assert.AreEqual(matrix1[x, y], matrix2[x, y]);
				}
			}
		}

		[TestMethod]
		public void TestMatrixCreationFromDimensionsSizeCorrect()
		{
			Matrix matrix = new Matrix(2, 3);

			Assert.AreEqual(matrix.Width, 2);
			Assert.AreEqual(matrix.Height, 3);
		}

		[TestMethod]
		public void TestMatrixCreationFromArraySizeCorrect()
		{
			Matrix matrix = new Matrix(new double[,]
			{
				{ 1, 2 },
				{ 3, 4 },
				{ 5, 6 }
			});

			Assert.AreEqual(matrix.Width, 2);
			Assert.AreEqual(matrix.Height, 3);
		}

		[TestMethod]
		public void TestMatrixMultiplication()
		{
			Matrix matrix1 = new Matrix(new double[,]
			{
				{ 1, 2 },
				{ 3, 4 },
				{ 5, 6 },
				{ 7, 8 }
			});

			Matrix matrix2 = new Matrix(new double[,]
			{
				{ 1, 2, 3 },
				{ 4, 5, 6 }
			});

			Matrix result = matrix1 * matrix2;

			Assert.AreEqual(matrix1.Height, result.Height);
			Assert.AreEqual(matrix2.Width, result.Width);
		}

		[TestMethod]
		public void TestMatrixMultiplicationCantMultiply()
		{
			Matrix matrix1 = new Matrix(new double[,]
			{
				{ 1, 2 },
				{ 3, 4 },
				{ 5, 6 },
				{ 7, 8 }
			});

			Matrix matrix2 = new Matrix(new double[,]
			{
				{ 1, 2, 3 },
				{ 4, 5, 6 }
			});

			Assert.ThrowsException<ArgumentException>(() =>
			{
				Matrix result = matrix2 * matrix1;
			});
		}
	}
}
