﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_App
{
	public static class MathHelper
	{
		public static double Clamp(double value, double min, double max)
		{
			return Math.Max(Math.Min(value, max), min);
		}

		public static double Lerp(double from, double to, double t)
		{
			return from + t * (to - from);
		}

		public static double Max(params double[] values)
		{
			double maxValue = Double.MinValue;
			foreach (double value in values)
			{
				if (value > maxValue)
				{
					maxValue = value;
				}
			}
			return maxValue;
		}

		public static double Min(params double[] values)
		{
			double minValue = Double.MaxValue;
			foreach (double value in values)
			{
				if (value < minValue)
				{
					minValue = value;
				}
			}
			return minValue;
		}
	}
}
