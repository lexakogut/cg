﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;

namespace CG_App
{
	static class ColorConverter
	{
		public static Color CMYK2RGB(ColorCMYK cmyk)
		{
			double c = cmyk.Cyan;
			double m = cmyk.Magenta;
			double y = cmyk.Yellow;
			double k = cmyk.Black;

			double oneMinusK = 1 - k;

			double r = (1.0 - c) * oneMinusK;
			double g = (1.0 - m) * oneMinusK;
			double b = (1.0 - y) * oneMinusK;

			int r255 = (int)Math.Round(255.0 * r);
			int g255 = (int)Math.Round(255.0 * g);
			int b255 = (int)Math.Round(255.0 * b);

			return Color.FromArgb(r255, g255, b255);
		}

		public static ColorCMYK RGB2CMYK(Color rgb)
		{
			double r = (rgb.R / 255.0);
			double g = (rgb.G / 255.0);
			double b = (rgb.B / 255.0);

			double maxRGB = MathHelper.Max(r, g, b);
			double oneMinusMaxRGB = (1 - maxRGB);
			double oneMinusMaxRGBClamped = Math.Max(oneMinusMaxRGB, 0);

			double k = oneMinusMaxRGBClamped;
			double oneMinusK = 1 - k;

			if (oneMinusK <= 0f)
			{
				return new ColorCMYK(0, 0, 0, 1);
			}

			double c = (1.0 - r - k) / oneMinusK;
			double m = (1.0 - g - k) / oneMinusK;
			double y = (1.0 - b - k) / oneMinusK;

			return new ColorCMYK((float)c, (float)m, (float)y, (float)k);
		}
	}
}
