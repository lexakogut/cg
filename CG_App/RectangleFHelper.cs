﻿using System;
using System.Drawing;
namespace CG_App
{
	public static class RectangleFHelper
	{
		public static RectangleF CreateRectangleFromCenter(float xCenter, float yCenter, float width, float height)
		{
			float xLeft = xCenter - width / 2f;
			float yTop = yCenter - height / 2f;
			return new RectangleF(xLeft, yTop, width, height);
		}
	}
}
