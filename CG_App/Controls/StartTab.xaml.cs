﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CG_App
{
	/// <summary>
	/// Interaction logic for StartTab.xaml
	/// </summary>
	public partial class StartTab : UserControl
	{
		public event EventHandler Button1Clicked;
		public event EventHandler Button2Clicked;
		public event EventHandler Button3Clicked;

		public StartTab()
		{
			InitializeComponent();
		}

		private void Button_GoToTab1_Click(object sender, RoutedEventArgs e)
		{
			Button1Clicked?.Invoke(this, e);
		}

		private void Button_GoToTab2_Click(object sender, RoutedEventArgs e)
		{
			Button2Clicked?.Invoke(this, e);
		}

		private void Button_GoToTab3_Click(object sender, RoutedEventArgs e)
		{
			Button3Clicked?.Invoke(this, e);
		}
	}
}
