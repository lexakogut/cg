﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Drawing.Imaging;

namespace CG_App
{
    /// <summary>
    /// Interaction logic for Tab1.xaml
    /// </summary>
    public partial class Tab2 : UserControl
    {
		private ImageEditor editor;
		private MyBitmap myBitmap;

		private Dictionary<string, Bitmap> savedImages;

        public Tab2()
        {
            InitializeComponent();
			InitEvents();

			editor = new ImageEditor();
			savedImages = new Dictionary<string, Bitmap>();

			Rectangle_Selection.Visibility = Visibility.Hidden;
			Rectangle_SelectedPixel.Visibility = Visibility.Hidden;
		}

		void InitEvents()
		{
			TextBox_CyanToChange.TextChanged += TextBox_CyanToChange_TextChanged;
			Button_Browse.Click += Button_Browse_Click;

			TextBox_X.TextChanged += TextBox_X_TextChanged;
			TextBox_Y.TextChanged += TextBox_Y_TextChanged;
			TextBox_W.TextChanged += TextBox_W_TextChanged;
			TextBox_H.TextChanged += TextBox_H_TextChanged;

			TextBox_PixelX.TextChanged += TextBox_PixelX_TextChanged;
			TextBox_PixelY.TextChanged += TextBox_PixelY_TextChanged;

			Canvas_ImageContainer.MouseDown += Canvas_ImageContainer_MouseDown;
			Canvas_ImageContainer.MouseMove += Canvas_ImageContainer_MouseMove;
			Canvas_ImageContainer.MouseLeftButtonUp += Canvas_ImageContainer_MouseLeftButtonUp;
			Canvas_ImageContainer.MouseRightButtonUp += Canvas_ImageContainer_MouseRightButtonUp;

			Button_Update.Click += Button_Update_Click;

			Button_Save.Click += Button_Save_Click;
			Button_Reset.Click += Button_Reset_Click;

			Slider_CyanToChange.ValueChanged += Slider_CyanToChange_ValueChanged;

			Button_Info.Click += Button_Info_Click;
			Popup_Info.Closed += Popup_Info_Closed;
		}

		private void Reset()
		{
			TextBox_X.Text = "0";
			TextBox_Y.Text = "0";
			TextBox_W.Text = "0";
			TextBox_H.Text = "0";

			TextBox_PixelX.Text = "0";
			TextBox_PixelY.Text = "0";

			TextBox_Cyan.Text = "0";
		}

		private System.Drawing.Point GetPointMappedToStretchedImage(System.Drawing.Point original)
		{
			double widthRatio = Canvas_ImageContainer.ActualWidth / Image_Source.Source.Width;
			double heightRatio = Canvas_ImageContainer.ActualHeight / Image_Source.Source.Height;

			int xMapped = (int)Math.Round(original.X / widthRatio);
			int yMapped = (int)Math.Round(original.Y / heightRatio);

			return new System.Drawing.Point(xMapped, yMapped);
		}

		private System.Drawing.Rectangle GetRectMappedToStretchedImage()
		{
			System.Drawing.Rectangle originalRect = new System.Drawing.Rectangle(x, y, w, h);

			System.Drawing.Point topLeftMapped = GetPointMappedToStretchedImage(new System.Drawing.Point(x, y));
			System.Drawing.Point bottomRightMapped = GetPointMappedToStretchedImage(new System.Drawing.Point(x + w, y + h));
			
			return System.Drawing.Rectangle.FromLTRB(
				topLeftMapped.X, topLeftMapped.Y, 
				bottomRightMapped.X, bottomRightMapped.Y);
		}

		private Bitmap SaveImage(string path, ImageFormat format)
		{
			Bitmap newBitmap = myBitmap.GenerateBitmap();
			newBitmap.Save(path, format);
			return newBitmap;
		}

		private void Button_Update_Click(object sender, RoutedEventArgs e)
		{
			if (myBitmap is null)
			{
				MessageBox.Show("No");
				return;
			}

			if (!Single.TryParse(TextBox_CyanToChange.Text, out float cyan))
			{
				MessageBox.Show("Can't parse cyan");
				return;
			}

			if (cyan < 0 || cyan > 1)
			{
				MessageBox.Show("Cyan out of range");
				return;
			}

			System.Drawing.Rectangle rect = GetRectMappedToStretchedImage();
			ICMYKEffect effect = new CMYKCyanEditEffect() { NewCyan = cyan };
			editor.ApplyCMYKEffectToArea(myBitmap, rect, effect);

			string filename = $"temp{savedImages.Count}.bmp";
			string globalPath = Directory.GetCurrentDirectory() + "/temp/" + filename;
			Bitmap newBitmap = SaveImage(globalPath, ImageFormat.Bmp);

			savedImages.Add(filename, newBitmap);
			SetImage(globalPath);
		}

		private void SetImage(string path)
		{
			Bitmap bitmap = System.Drawing.Image.FromFile(path) as Bitmap;
			myBitmap = MyBitmap.FromGDIBitmap(bitmap);

			BitmapImage image = new BitmapImage(new Uri(path));
			Image_Source.Width = Canvas_ImageContainer.ActualWidth;
			Image_Source.Height = Canvas_ImageContainer.ActualHeight;
			Image_Source.Source = image;
		}

		private void Button_Browse_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog dialog = new OpenFileDialog();
			if (dialog.ShowDialog() == true)
			{
				string path = dialog.FileName;
				SetImage(path);
				TextBox_Path.Text = path;
			}
		}

		int mouseX1, mouseY1;
		int mouseX2, mouseY2;
		int x, y, w, h, px, py;

		private void UpdateSelectionRectangle()
		{
			if (myBitmap is null)
			{
				return;
			}

			Rectangle_Selection.Visibility = Visibility.Visible;

			if (!Int32.TryParse(TextBox_X.Text, out x)) return;
			if (!Int32.TryParse(TextBox_Y.Text, out y)) return;
			if (!Int32.TryParse(TextBox_W.Text, out w)) return;
			if (!Int32.TryParse(TextBox_H.Text, out h)) return;

			Rectangle_Selection.Width = w;
			Rectangle_Selection.Height = h;
			Canvas.SetLeft(Rectangle_Selection, x);
			Canvas.SetTop(Rectangle_Selection, y);
		}
		
		private void TextBox_X_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_X.Text, out x)) return;

			UpdateSelectionRectangle();
		}

		private void TextBox_Y_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_Y.Text, out y)) return;

			UpdateSelectionRectangle();
		}

		private void TextBox_W_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_W.Text, out w)) return;

			UpdateSelectionRectangle();
		}

		private void TextBox_H_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_H.Text, out h)) return;

			UpdateSelectionRectangle();
		}

		private void UpdateSelectionPixel()
		{
			if (myBitmap is null)
			{
				return;
			}
			
			if (!Int32.TryParse(TextBox_PixelX.Text, out px)) return;
			if (!Int32.TryParse(TextBox_PixelY.Text, out py)) return;
			System.Drawing.Point convertedPoint = GetPointMappedToStretchedImage(new System.Drawing.Point(px, py));

			if (!myBitmap.IsInBounds(convertedPoint.X, convertedPoint.Y))
			{
				return;
			}

			Rectangle_SelectedPixel.Visibility = Visibility.Visible;

			System.Drawing.Color fillColor = myBitmap[convertedPoint.X, convertedPoint.Y];
			System.Drawing.Color borderColor = System.Drawing.Color.FromArgb(255 - fillColor.R, 255 - fillColor.G, 255 - fillColor.B);

			double w = Rectangle_SelectedPixel.ActualWidth;
			double h = Rectangle_SelectedPixel.ActualHeight;
			Canvas.SetLeft(Rectangle_SelectedPixel, px - w / 2);
			Canvas.SetTop(Rectangle_SelectedPixel, py - h / 2);
			Rectangle_SelectedPixel.Fill = new SolidColorBrush(System.Windows.Media.Color.FromArgb(fillColor.A, fillColor.R, fillColor.G, fillColor.B));
			Rectangle_SelectedPixel.Stroke = new SolidColorBrush(System.Windows.Media.Color.FromArgb(borderColor.A, borderColor.R, borderColor.G, borderColor.B));

			UpdateRGBAndCYANFields(fillColor);
		}

		private void TextBox_PixelX_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_PixelX.Text, out px)) return;
			UpdateSelectionPixel();
		}

		private void TextBox_PixelY_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Int32.TryParse(TextBox_PixelY.Text, out py)) return;
			UpdateSelectionPixel();
		}

		private void UpdateRGBAndCYANFields(System.Drawing.Color rgb)
		{
			ColorCMYK cmyk = ColorConverter.RGB2CMYK(rgb);
			
			TextBox_Hex.Text = ColorTranslator.ToHtml(rgb);
			TextBox_Red.Text = rgb.R.ToString();
			TextBox_Green.Text = rgb.G.ToString();
			TextBox_Blue.Text = rgb.B.ToString();

			TextBox_Cyan.Text = cmyk.Cyan.ToString("G2");
			TextBox_Magenta.Text = cmyk.Magenta.ToString("G2");
			TextBox_Yellow.Text = cmyk.Yellow.ToString("G2");
			TextBox_Black.Text = cmyk.Black.ToString("G2");
		}

		private void Canvas_ImageContainer_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed)
			{
				System.Windows.Point point = e.GetPosition(Canvas_ImageContainer);
				mouseX1 = (int)point.X;
				mouseY1 = (int)point.Y;
				mouseX2 = mouseX1;
				mouseY2 = mouseY1;
			}
		}
		
		private void UpdateMousePositionVariables()
		{
			System.Windows.Point point = Mouse.GetPosition(Canvas_ImageContainer);
			mouseX2 = (int)point.X;
			mouseY2 = (int)point.Y;
		}

		private void UpdatePixelSelectionTextBoxes()
		{
			TextBox_PixelX.Text = mouseX2.ToString();
			TextBox_PixelY.Text = mouseY2.ToString();
		}

		private void UpdateRectangleSelectionTextBoxes()
		{
			int xMin = Math.Min(mouseX1, mouseX2);
			int yMin = Math.Min(mouseY1, mouseY2);

			int w = Math.Abs(mouseX1 - mouseX2);
			int h = Math.Abs(mouseY1 - mouseY2);

			TextBox_X.Text = xMin.ToString();
			TextBox_Y.Text = yMin.ToString();
			TextBox_W.Text = w.ToString();
			TextBox_H.Text = h.ToString();
		}
		
		private void Canvas_ImageContainer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			UpdateMousePositionVariables();
			UpdateRectangleSelectionTextBoxes();
		}

		private void Canvas_ImageContainer_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			UpdateMousePositionVariables();
			UpdatePixelSelectionTextBoxes();
		}

		private void Canvas_ImageContainer_MouseMove(object sender, MouseEventArgs e)
		{
			UpdateMousePositionVariables();

			if (e.LeftButton == MouseButtonState.Pressed)
			{
				UpdateRectangleSelectionTextBoxes();
			}

			if (e.RightButton == MouseButtonState.Pressed)
			{
				UpdatePixelSelectionTextBoxes();
			}
		}

		private void TextBox_CyanToChange_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!Single.TryParse(TextBox_CyanToChange.Text, out float cyan) || cyan < 0 || cyan > 1)
			{
				return;
			}

			Slider_CyanToChange.Value = cyan;
		}

		private void Slider_CyanToChange_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			TextBox_CyanToChange.Text = Slider_CyanToChange.Value.ToString("G2");
		}

		private void Popup_Info_Closed(object sender, EventArgs e)
		{
			Rectangle_PopupMask.Visibility = Visibility.Hidden;
		}

		private void Button_Info_Click(object sender, RoutedEventArgs e)
		{
			Popup_Info.IsOpen = true;
			Rectangle_PopupMask.Visibility = Visibility.Visible;
		}

		private void Button_Reset_Click(object sender, RoutedEventArgs e)
		{
			Reset();
		}
		
		private void Button_Save_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog dialog = new SaveFileDialog();
			if (dialog.ShowDialog() == true)
			{
				string path = dialog.FileName;
				switch (System.IO.Path.GetExtension(path))
				{
					case ".jpg":
					case ".jpeg":
						SaveImage(path, ImageFormat.Jpeg);
						break;
					case ".png":
						SaveImage(path, ImageFormat.Png);
						break;
					case ".bmp":
						SaveImage(path, ImageFormat.Bmp);
						break;
					default:
						MessageBox.Show("Unknown file format");
						break;
				}
			}
		}
	}
}
