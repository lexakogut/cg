﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CG_App
{
    /// <summary>
    /// Interaction logic for SideTab.xaml
    /// </summary>
    public partial class SideTab : UserControl
    {
		public event EventHandler Menu1ButtonClicked;
		public event EventHandler Menu2ButtonClicked;
		public event EventHandler Menu3ButtonClicked;
		public event EventHandler Menu4ButtonClicked;

		public SideTab()
        {
            InitializeComponent();
        }

		private void Button_Option1_Click(object sender, RoutedEventArgs e)
		{
			Menu1ButtonClicked?.Invoke(this, e);
		}

		private void Button_Option2_Click(object sender, RoutedEventArgs e)
		{
			Menu2ButtonClicked?.Invoke(this, e);
		}

		private void Button_Option3_Click(object sender, RoutedEventArgs e)
		{
			Menu3ButtonClicked?.Invoke(this, e);
		}

		private void Button_Option4_Click(object sender, RoutedEventArgs e)
		{
			Menu4ButtonClicked?.Invoke(this, e);
		}

		private void Grid_MouseEnter(object sender, MouseEventArgs e)
		{

		}
	}
}
