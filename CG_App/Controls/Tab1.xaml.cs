﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CG_App
{
    /// <summary>
    /// Interaction logic for Tab1.xaml
    /// </summary>
    public partial class Tab1 : UserControl
    {
		private FractalDrawer FractalDrawer;
		const float WindowAreaPart = 50.0f;

		BackgroundWorker Worker;

		private readonly ColorScheme[] colorSchemes = new ColorScheme[]
		{
			new ColorScheme("Red dragon",
				System.Drawing.Color.Red,
				System.Drawing.Color.Yellow,
				System.Drawing.Color.Black),
			new ColorScheme("Blue ice",
				System.Drawing.Color.FromArgb(0, 0, 20),
				System.Drawing.Color.Blue,
				System.Drawing.Color.GhostWhite),
			new ColorScheme("Magenta neon",
				System.Drawing.Color.Black,
				System.Drawing.Color.Magenta,
				System.Drawing.Color.DarkMagenta)
		};

		public Tab1()
		{
			InitializeComponent();

			RectangleF rect = RectangleFHelper.CreateRectangleFromCenter(0, 0, 1f, 1f);
			FractalDrawer = new FractalDrawer(rect, colorSchemes[0]);
			Worker = new BackgroundWorker();

			TextBox_Iterations.TextChanged += TextBox_Iterations_TextChanged;
			Slider_Iterations.ValueChanged += Slider_Iterations_ValueChanged;
			TextBox_X.TextChanged += TextBox_X_TextChanged;
			TextBox_Y.TextChanged += TextBox_Y_TextChanged;
			TextBox_Zoom.TextChanged += TextBox_Zoom_TextChanged;
			Slider_Zoom.ValueChanged += Slider_Zoom_ValueChanged;
			ComboBox_ColorScheme.SelectionChanged += ComboBox_ColorScheme_SelectionChanged;
			Button_Draw.Click += Button_Draw_Click;
			Button_Info.Click += Button_Info_Click;
			Popup_Info.Closed += Popup_Info_Closed;

			string path = String.Concat(Directory.GetCurrentDirectory(), "/loading.gif");
			LoadingImage.Source = new Uri(path);
			LoadingImage.Opacity = 0;

			Slider_Zoom.Value = 0.0f;
			UpdateAreaFromForms();
		}

		void PutPixelsAndRenderBitmap(int width, int height, WriteableBitmap bitmap, byte[] pixelsBGR)
		{
			int stride = width * 4;
			int offset = 0;
			Int32Rect rect = new Int32Rect(0, 0, width, height);

			bitmap.Lock();
			bitmap.WritePixels(rect, pixelsBGR, stride, offset);
			bitmap.AddDirtyRect(rect);
			bitmap.Unlock();

			FractalImage.Source = bitmap;
		}
		
		class WorkResult
		{
			public int width;
			public int height;
			public byte[] pixelsBGR;

			public WorkResult(int width, int height, byte[] pixelsBGR)
			{
				this.width = width;
				this.height = height;
				this.pixelsBGR = pixelsBGR;
			}
		}

		void CalculateBGRWork(object sender, DoWorkEventArgs workArgs)
		{
			int width = (int)FractalWindow.ActualWidth;
			int height = (int)FractalWindow.ActualHeight;
			System.Drawing.Color[,] fractalPixels = FractalDrawer.RenderFractal(width, height);
			MyBitmap fractalBitmap = new MyBitmap(fractalPixels);
			byte[] pixelsBGR = fractalBitmap.GenerateBGRArray();
			workArgs.Result = new WorkResult(width, height, pixelsBGR);
		}

		void DrawFractalWork(object sender, RunWorkerCompletedEventArgs workArgs)
		{
			WorkResult result = workArgs.Result as WorkResult;
			DrawFractal(result.width, result.height, result.pixelsBGR);
		}

		void DrawFractal(int width, int height, byte[] pixelsBGR)
		{
			WriteableBitmap writeableBitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgr32, null);
			PutPixelsAndRenderBitmap(width, height, writeableBitmap, pixelsBGR);
			LoadingImage.Opacity = 0;
			Button_Draw.IsEnabled = true;
		}

		private void Button_Draw_Click(object sender, RoutedEventArgs e)
		{
			Button_Draw.IsEnabled = false;
			UpdateAreaFromForms();
			LoadingImage.Opacity = 100;
			Worker.DoWork += CalculateBGRWork;
			Worker.RunWorkerCompleted += DrawFractalWork;
			Worker.RunWorkerAsync();
		}

		void ChangeIterations(int iterations)
		{
			FractalDrawer.MaxIterations = iterations;
		}

		private void Slider_Iterations_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			int iterations = (int)e.NewValue;
			ChangeIterations(iterations);
			TextBox_Iterations.Text = iterations.ToString();
		}

		private void TextBox_Iterations_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Int32.TryParse(TextBox_Iterations.Text, out int iterations))
			{
				ChangeIterations(iterations);
				Slider_Iterations.Value = iterations;
			}
		}

		void ChangeArea(float x, float y, float zoom)
		{
			float width = (float)FractalWindow.ActualWidth / WindowAreaPart / zoom;
			float height = (float)FractalWindow.ActualHeight / WindowAreaPart / zoom;

			FractalDrawer.Area = RectangleFHelper.CreateRectangleFromCenter(x, y, width, height);
		}

		void UpdateAreaFromForms()
		{
			float x, y, zoom;
			if (!Single.TryParse(TextBox_X.Text, out x)) return;
			if (!Single.TryParse(TextBox_Y.Text, out y)) return;
			if (!Single.TryParse(TextBox_Zoom.Text, out zoom)) return;

			ChangeArea(x, y, zoom);
		}

		private void TextBox_X_TextChanged(object sender, TextChangedEventArgs e)
		{
			UpdateAreaFromForms();
		}

		private void TextBox_Y_TextChanged(object sender, TextChangedEventArgs e)
		{
			UpdateAreaFromForms();
		}

		private void TextBox_Zoom_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (Single.TryParse(TextBox_Zoom.Text, out float zoom))
			{
				Slider_Zoom.Value = Math.Log10(zoom);
			}

			UpdateAreaFromForms();
		}

		private void Slider_Zoom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			float zoom = (float)Math.Pow(10, e.NewValue);
			TextBox_Zoom.Text = zoom.ToString();

			UpdateAreaFromForms();
		}

		private void ComboBox_ColorScheme_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			int index = ComboBox_ColorScheme.SelectedIndex;

			FractalDrawer.ColorScheme = colorSchemes[index];
		}

		private void Popup_Info_Closed(object sender, EventArgs e)
		{
			Rectangle_PopupMask.Visibility = Visibility.Hidden;
		}

		private void Button_Info_Click(object sender, RoutedEventArgs e)
		{
			Popup_Info.IsOpen = true;
			Rectangle_PopupMask.Visibility = Visibility.Visible;
		}
	}
}
