﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Drawing.Imaging;
using Point = System.Windows.Point;

namespace CG_App
{
	/// <summary>
	/// Interaction logic for Tab1.xaml
	/// </summary>
	public partial class Tab3 : UserControl
	{
		class TriangleContext
		{
			public Polygon Polygon;
			public Label LabelA, LabelB, LabelC;
			public Triangle Triangle;

			public TriangleContext(Polygon polygon, Label labelA, Label labelB, Label labelC, Triangle triangle)
			{
				this.Polygon = polygon;
				this.LabelA = labelA;
				this.LabelB = labelB;
				this.LabelC = labelC;
				this.Triangle = triangle;
			}

			public void Show()
			{
				Polygon.Visibility = Visibility.Visible;

				if (LabelA != null) LabelA.Visibility = Visibility.Visible;
				if (LabelB != null) LabelB.Visibility = Visibility.Visible;
				if (LabelC != null) LabelC.Visibility = Visibility.Visible;
			}

			public void Hide()
			{
				Polygon.Visibility = Visibility.Collapsed;

				if (LabelA != null) LabelA.Visibility = Visibility.Collapsed;
				if (LabelB != null) LabelB.Visibility = Visibility.Collapsed;
				if (LabelC != null) LabelC.Visibility = Visibility.Collapsed;
			}
		}
		class TextBoxAndSliderBinding
		{
			TextBox textBox;
			Slider slider;

			public event EventHandler<double> ValueUpdated;

			public double Value => slider.Value;
			private bool skipRecursiveChange = false;

			public TextBoxAndSliderBinding(TextBox textBox, Slider slider)
			{
				this.textBox = textBox;
				this.slider = slider;

				this.textBox.TextChanged += TextBox_TextChanged;
				this.slider.ValueChanged += Slider_ValueChanged;
			}

			private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
			{
				if (skipRecursiveChange) return;

				if (Double.TryParse(textBox.Text, out double value))
				{
					if (value > slider.Maximum)
					{
						value = slider.Maximum;
					}
					if (value < slider.Minimum)
					{
						value = slider.Minimum;
					}

					UpdateSlider(value);
				}
				ValueUpdated?.Invoke(this, Value);
			}

			private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
			{
				if (skipRecursiveChange) return;

				UpdateTextBox(slider.Value.ToString("G4"));
				ValueUpdated?.Invoke(this, Value);
			}

			private void UpdateTextBox(string valueText)
			{
				skipRecursiveChange = true;
				textBox.Text = valueText;
				skipRecursiveChange = false;
			}

			private void UpdateSlider(double value)
			{
				skipRecursiveChange = true;
				slider.Value = value;
				skipRecursiveChange = false;
			}
		}

		double AreaToPixelMultiplier = 40.0;
		double AxisOffsetInAreaUnits = 5.0;
		
		TriangleContext TriangleOriginal, TriangleConverted;
		TriangleBaseTransformator transformator;
		List<TextBoxAndSliderBinding> coordBindings;
		TextBoxAndSliderBinding sizeBinding;
		TextBoxAndSliderBinding heightBinding;

		public Tab3()
		{
			InitializeComponent();

			transformator = new TriangleBaseTransformator();

			InitTextBoxAndSliderBindings();
			InitEvents();
			UpdateAxis();
			InitTriangleContexts();
			ResetMainTriangleOptions();
			UpdateConvertedTriangleAppearance();
		}

		private void InitTextBoxAndSliderBindings()
		{
			coordBindings = new List<TextBoxAndSliderBinding>
			{
				new TextBoxAndSliderBinding(TextBox_Ax, Slider_Ax),
				new TextBoxAndSliderBinding(TextBox_Ay, Slider_Ay),
				new TextBoxAndSliderBinding(TextBox_Bx, Slider_Bx),
				new TextBoxAndSliderBinding(TextBox_By, Slider_By)
			};

			sizeBinding = new TextBoxAndSliderBinding(TextBox_ConvertedSize, Slider_ConvertedSize);
			heightBinding = new TextBoxAndSliderBinding(TextBox_Height, Slider_Height);
		}

		private void UpdateAxis()
		{
			AxisX.X1 = 0;
			AxisX.X2 = 9999;
			AxisX.Y1 = AxisOffsetInAreaUnits * AreaToPixelMultiplier;
			AxisX.Y2 = AxisOffsetInAreaUnits * AreaToPixelMultiplier;
			AxisY.X1 = AxisOffsetInAreaUnits * AreaToPixelMultiplier;
			AxisY.X2 = AxisOffsetInAreaUnits * AreaToPixelMultiplier;
			AxisY.Y1 = 0;
			AxisY.Y2 = 9999;
		}

		private void InitEvents()
		{
			Slider_ConvertedSize.ValueChanged += Slider_Animation_ValueChanged;
			Button_Reset.Click += Button_Reset_Click;
			CheckBox_HideAnimation.Click += CheckBox_HideAnimation_Click;

			foreach (TextBoxAndSliderBinding binding in coordBindings)
			{
				binding.ValueUpdated += CoordBinding_ValueUpdated;
			}
			heightBinding.ValueUpdated += CoordBinding_ValueUpdated;
			sizeBinding.ValueUpdated += AnimationBinding_ValueUpdated;

			CheckBox_Clockwise.Click += CheckBox_Clockwise_Click;
			ComboBox_Side.SelectionChanged += ComboBox_Side_SelectionChanged;

			Button_Info.Click += Button_Info_Click;

			Popup_Info.Closed += Popup_Info_Closed;
		}

		private void Popup_Info_Closed(object sender, EventArgs e)
		{
			Rectangle_PopupMask.Visibility = Visibility.Hidden;
		}

		private void Button_Info_Click(object sender, RoutedEventArgs e)
		{
			Popup_Info.IsOpen = true;
			Rectangle_PopupMask.Visibility = Visibility.Visible;
		}

		private void ComboBox_Side_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			TryDrawTriangle();
		}

		private void CheckBox_Clockwise_Click(object sender, RoutedEventArgs e)
		{
			TryDrawTriangle();
		}

		private void CoordBinding_ValueUpdated(object sender, double e)
		{
			TryDrawTriangle();
		}

		private void TryDrawTriangle()
		{
			Label_Error.Visibility = Visibility.Hidden;
			try
			{
				Vector2 a = GetAPosition();
				Vector2 b = GetBPosition();

				double height = Slider_Height.Value;
				bool clockwise = CheckBox_Clockwise.IsChecked.HasValue ? CheckBox_Clockwise.IsChecked.Value : false;
				Vector2 c = CalculateCPosition(a, b, height, clockwise);

				SetCTextboxes(c);
				SetTriangleAPoints(a, b, c);
				DrawPolygonTriangle(TriangleOriginal);
				MakeTransformations();

				ExtendLine(a, b, out Vector2 exA, out Vector2 exB);
				DrawMainLine(exA, exB);
			}
			catch (Exception ex)
			{
				Label_Error.Content = ex.Message;
				Label_Error.Visibility = Visibility.Visible;
			}
		}

		private Vector2 GetAPosition()
		{
			double ax = Double.Parse(TextBox_Ax.Text);
			double ay = Double.Parse(TextBox_Ay.Text);

			return new Vector2(ax, ay);
		}

		private Vector2 GetBPosition()
		{
			double bx = Double.Parse(TextBox_Bx.Text);
			double by = Double.Parse(TextBox_By.Text);

			return new Vector2(bx, by);
		}

		private void SetCTextboxes(Vector2 c)
		{
			TextBox_Cx.Text = c.X.ToString("G4");
			TextBox_Cy.Text = c.Y.ToString("G4");
		}

		private void ExtendLine(Vector2 a, Vector2 b, out Vector2 exA, out Vector2 exB)
		{
			Vector2 direction = Vector2.Direction(a, b);
			exA = a + direction * 9999;
			exB = b - direction * 9999;
		}

		private void InitTriangleContexts()
		{
			PolygonOriginal.Points.Add(new Point());
			PolygonOriginal.Points.Add(new Point());
			PolygonOriginal.Points.Add(new Point());
			
			PolygonConverted.Points.Add(new Point());
			PolygonConverted.Points.Add(new Point());
			PolygonConverted.Points.Add(new Point());

			TriangleOriginal = new TriangleContext(PolygonOriginal, LabelA1, LabelB1, LabelC1, new Triangle());
			TriangleConverted = new TriangleContext(PolygonConverted, LabelA2, LabelB2, LabelC2, new Triangle());
		}

		double AreaCoordsToCanvas(double value)
		{
			return (value + AxisOffsetInAreaUnits) * AreaToPixelMultiplier;
		}

		private void DrawPolygonTriangle(TriangleContext context)
		{
			double ax = AreaCoordsToCanvas(context.Triangle.A.X);
			double ay = AreaCoordsToCanvas(context.Triangle.A.Y);
			double bx = AreaCoordsToCanvas(context.Triangle.B.X);
			double by = AreaCoordsToCanvas(context.Triangle.B.Y);
			double cx = AreaCoordsToCanvas(context.Triangle.C.X);
			double cy = AreaCoordsToCanvas(context.Triangle.C.Y);

			context.Polygon.Points[0] = new Point(ax, ay);
			context.Polygon.Points[1] = new Point(bx, by);
			context.Polygon.Points[2] = new Point(cx, cy);

			if (context.LabelA != null)
			{
				Canvas.SetLeft(context.LabelA, ax);
				Canvas.SetTop(context.LabelA, ay);
			}

			if (context.LabelB != null)
			{
				Canvas.SetLeft(context.LabelB, bx);
				Canvas.SetTop(context.LabelB, by);
			}

			if (context.LabelC != null)
			{
				Canvas.SetLeft(context.LabelC, cx); 
				Canvas.SetTop(context.LabelC, cy);
			}

			context.Show();
		}

		private void ResetMainTriangleOptions()
		{
			Slider_Ax.Value = -2;
			Slider_Ay.Value = -1;
			Slider_Bx.Value = 2;
			Slider_By.Value = -3;
			Slider_Height.Value = 2;
			Slider_ConvertedSize.Value = 0.5;
		}

		private Vector2 CalculateCPosition(Vector2 a, Vector2 b, double h, bool clockwise)
		{
			Vector2 heightIntersection = (a + b) / 2.0;

			Vector2 directionAB = Vector2.Direction(a, b);

			double angleAB = directionAB.Slope;
			double rotatedAngle = clockwise ? (angleAB + Math.PI / 2) : (angleAB - Math.PI / 2);

			Vector2 heightDirection = Vector2.Polar(rotatedAngle, h);

			Vector2 c = heightIntersection + heightDirection;

			return c;
		}

		private void SetTriangleAPoints(Vector2 a, Vector2 b, Vector2 c)
		{
			TriangleOriginal.Triangle = new Triangle(a, b, c);
		}

		private void DrawMainLine(Vector2 a, Vector2 b)
		{
			double x1Canvas = AreaCoordsToCanvas(a.X);
			double y1Canvas = AreaCoordsToCanvas(a.Y);
			double x2Canvas = AreaCoordsToCanvas(b.X);
			double y2Canvas = AreaCoordsToCanvas(b.Y);
			
			MainSideLine.X1 = x1Canvas;
			MainSideLine.Y1 = y1Canvas;
			MainSideLine.X2 = x2Canvas;
			MainSideLine.Y2 = y2Canvas;
		}
		
		private void AnimationBinding_ValueUpdated(object sender, double e)
		{
			MakeTransformations();
		}

		private Triangle.Side GetTriangleSideFromComboBox()
		{
			switch (ComboBox_Side.SelectedIndex)
			{
				case 0:
					return Triangle.Side.AB;
				case 1:
					return Triangle.Side.BC;
				case 2:
					return Triangle.Side.CA;
				default:
					return Triangle.Side.AB;
			}
		}

		private void MakeTransformationWithPolygon(TriangleContext context, double animationValue)
		{
			Triangle.Side side = GetTriangleSideFromComboBox();
			context.Triangle = transformator.Transform(TriangleOriginal.Triangle, side, animationValue);
			DrawPolygonTriangle(context);
		}

		private void Slider_Animation_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			MakeTransformations();
		}

		private void MakeTransformations()
		{
			if (IsAnimatedTriangleEnabled)
			{
				double value = Slider_ConvertedSize.Value;
				MakeTransformationWithPolygon(TriangleConverted, value);
			}
		}

		private void Button_Reset_Click(object sender, RoutedEventArgs e)
		{
			ResetMainTriangleOptions();
		}

		private void CheckBox_HideAnimation_Click(object sender, RoutedEventArgs e)
		{
			UpdateConvertedTriangleAppearance();
		}

		private void UpdateConvertedTriangleAppearance()
		{
			if (IsAnimatedTriangleEnabled)
			{
				MakeTransformations();
				Slider_ConvertedSize.IsEnabled = true;
				TriangleConverted.Show();
			}
			else
			{
				Slider_ConvertedSize.IsEnabled = false;
				TriangleConverted.Hide();
			}
		}

		private bool IsAnimatedTriangleEnabled
		{
			get => CheckBox_HideAnimation.IsChecked.HasValue && (CheckBox_HideAnimation.IsChecked.Value == true);
		}
	}
}
