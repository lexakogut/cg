﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_App
{
	public struct ColorCMYK
	{
		private float cyan;
		public float Cyan
		{
			get => cyan;
			set
			{
				ValueCheck(value);
				cyan = value;
			}
		}
		
		private float magenta;
		public float Magenta
		{
			get => magenta;
			set
			{
				ValueCheck(value);
				magenta = value;
			}
		}

		private float yellow;
		public float Yellow
		{
			get => yellow;
			set
			{
				ValueCheck(value);
				yellow = value;
			}
		}

		private float black;
		public float Black
		{
			get => black;
			set
			{
				ValueCheck(value);
				black = value;
			}
		}

		private void ValueCheck(float value)
		{
			if (value > 1 || value < 0)
			{
				throw new ArgumentOutOfRangeException("Value must be in range [0; 1]");
			}
		}
		
		public ColorCMYK(float cyan, float magenta, float yellow, float black) : this()
		{
			Cyan = cyan;
			Magenta = magenta;
			Yellow = yellow;
			Black = black;
		}
	}
}
