﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace CG_App
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			InitEvents();

			SwitchToStartMenu();
		}

		private void InitEvents()
		{
			SideTab.Menu1ButtonClicked += (sender, e) =>
			{
				SwitchToFractalMenu();
			};

			SideTab.Menu2ButtonClicked += (sender, e) =>
			{
				SwitchToColorMenu();
			};

			SideTab.Menu3ButtonClicked += (sender, e) =>
			{
				SwitchToAffineMenu();
			};

			SideTab.Menu4ButtonClicked += (sender, e) =>
			{
				SwitchToStartMenu();
			};

			StartTab.Button1Clicked += (sender, e) =>
			{
				SwitchToFractalMenu();
			};

			StartTab.Button2Clicked += (sender, e) =>
			{
				SwitchToColorMenu();
			};

			StartTab.Button3Clicked += (sender, e) =>
			{
				SwitchToAffineMenu();
			};
		}

		private void SwitchToFractalMenu()
		{
			MenuTab1.Visibility = Visibility.Visible;
			MenuTab2.Visibility = Visibility.Collapsed;
			MenuTab3.Visibility = Visibility.Collapsed;
			StartTab.Visibility = Visibility.Collapsed;
			SideTab.Visibility = Visibility.Visible;
		}

		private void SwitchToColorMenu()
		{
			MenuTab1.Visibility = Visibility.Collapsed;
			MenuTab2.Visibility = Visibility.Visible;
			MenuTab3.Visibility = Visibility.Collapsed;
			StartTab.Visibility = Visibility.Collapsed;
			SideTab.Visibility = Visibility.Visible;
		}

		private void SwitchToAffineMenu()
		{
			MenuTab1.Visibility = Visibility.Collapsed;
			MenuTab2.Visibility = Visibility.Collapsed;
			MenuTab3.Visibility = Visibility.Visible;
			StartTab.Visibility = Visibility.Collapsed;
			SideTab.Visibility = Visibility.Visible;
		}

		private void SwitchToStartMenu()
		{
			MenuTab1.Visibility = Visibility.Collapsed;
			MenuTab2.Visibility = Visibility.Collapsed;
			MenuTab3.Visibility = Visibility.Collapsed;
			StartTab.Visibility = Visibility.Visible;
			SideTab.Visibility = Visibility.Collapsed;
		}
	}
}
