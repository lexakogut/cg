﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;

namespace CG_App
{
	class ImageEditor
	{
		public void ApplyCMYKEffectToArea(MyBitmap bitmap, Rectangle area, ICMYKEffect cmykEffect)
		{
			int startX = Math.Min(area.Left, area.Right);
			int endX = Math.Max(area.Left, area.Right);

			int startY = Math.Min(area.Bottom, area.Top);
			int endY = Math.Max(area.Bottom, area.Top);

			for (int y = startY; y <= endY; ++y)
			{
				for (int x = startX; x <= endX; ++x)
				{
					if (!bitmap.IsInBounds(x, y)) continue;

					Color rgb = bitmap[x, y];
					ColorCMYK cmyk = ColorConverter.RGB2CMYK(rgb);
					ColorCMYK newCmyk = cmykEffect.ApplyEffect(cmyk);
					bitmap[x, y] = ColorConverter.CMYK2RGB(newCmyk);
				}
			}
		}
	}
}
