﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using System.Threading.Tasks;

namespace CG_App
{
	class FractalDrawer
	{
		public RectangleF Area { get; set; }
		public ColorScheme ColorScheme { get; set; }
		public int MaxIterations { get; set; }
		public float MaxValueDistance { get; set; }

		public FractalDrawer(RectangleF area, ColorScheme colorScheme, int maxIterations = 500, float maxValueDistance = 2.0f)
		{
			Area = area;
			ColorScheme = colorScheme;
			MaxIterations = maxIterations;
			MaxValueDistance = maxValueDistance;
		}

		public Color[,] RenderFractal(int bitmapWidth, int bitmapHeight)
		{
			Color[,] bitmap = new Color[bitmapHeight, bitmapWidth];
			Size bitmapSize = new Size(bitmapWidth, bitmapHeight);

			PointF[,] truePixelPositions = new PointF[bitmapHeight, bitmapWidth];
			
			for (int y = 0; y < bitmapHeight; ++y)
			{
				for (int x = 0; x < bitmapWidth; ++x)
				{
					truePixelPositions[y, x] = BitmapPixelToAreaPixel(new Point(x, y), bitmapSize);
				}
			}

			for (int y = 0; y < bitmapHeight; ++y)
			{
				Parallel.For(0, bitmapWidth, (x) =>
				{
					bitmap[y, x] = CalculatePixelColor(truePixelPositions[y, x]);
				});
			}

			return bitmap;
		}

		PointF BitmapPixelToAreaPixel(Point pixel, Size bitmapSize)
		{
			float xRel = (float)pixel.X / bitmapSize.Width;
			float yRel = (float)pixel.Y / bitmapSize.Height;

			float x = xRel * Area.Width + Area.X;
			float y = yRel * Area.Height + Area.Y;

			return new PointF(x, y);
		}

		Complex Mandelbrot(Complex z, Complex c)
		{
			return z * z + c;
		}

		Complex TanZ1(Complex z, Complex c)
		{
			return z * Complex.Tan(z) + c;
		}
		
		Complex TanZ2(Complex z, Complex c)
		{
			return Complex.Tan(z) + c;
		}

		Complex Julia(Complex z, Complex c)
		{
			double ca = c.Real;
			double cb = c.Imaginary;
			double a = z.Real;
			double b = z.Imaginary;

			double aa = a * a;
			double bb = b * b;
			double twoab = 2.0 * a * b;

			double new_a = aa - bb + ca;
			double new_b = twoab + cb;

			return new Complex(new_a, new_b);
		}

		Color CalculatePixelColor(PointF pixel)
		{
			Complex z = new Complex(0, 0);
			Complex c = new Complex(pixel.X, pixel.Y);

			for (int iteration = 0; iteration < MaxIterations; ++iteration)
			{
				z = TanZ1(z, c);
				if (z.Magnitude >= MaxValueDistance)
				{
					return CalculateColorFromIteration(iteration);
				}
			}

			return ColorScheme.EscapeColor;
		}

		Color CalculateColorFromIteration(int iteration)
		{
			float blendValue = (float)Math.Sqrt((float)iteration / MaxIterations);

			Color min = ColorScheme.MinColor;
			Color max = ColorScheme.MaxColor;
			Color mixed = Color.FromArgb(
				(byte)(min.R + (max.R - min.R) * blendValue),
				(byte)(min.G + (max.G - min.G) * blendValue),
				(byte)(min.B + (max.B - min.B) * blendValue));

			return mixed;
		}
	}
}
