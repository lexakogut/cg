﻿using System;
using System.Drawing;

namespace CG_App
{
    class ColorScheme
    {
		public string Name { get; set; }
		public Color MinColor { get; set; }
		public Color MaxColor { get; set; }
		public Color EscapeColor { get; set; }

		public ColorScheme(string name, Color minColor, Color maxColor, Color escapeColor)
		{
			Name = name;
			MinColor = minColor;
			MaxColor = maxColor;
			EscapeColor = escapeColor;
		}
	}
}
