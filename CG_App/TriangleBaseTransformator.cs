﻿using System;
using CG_App.Tools;

namespace CG_App
{
	/*
	 * Задано рівнобедрений трикутник через координати вершин. Реалізувати рух
	 * трикутника на основі дзеркального відображення відносно прямої, проведеної через
	 * основу трикутника з одночасним зменшенням у 2 рази.
	 */
	class TriangleBaseTransformator
	{
		private AffineTransformator affineTransformator;

		public TriangleBaseTransformator()
		{
			this.affineTransformator = new AffineTransformator();
		}

		public Triangle Transform(Triangle original, Triangle.Side side, double scaleParameter)
		{
			Matrix triangleMatrix = new Matrix(new double[,]
			{
				{ original.A.X, original.A.Y, 1 },
				{ original.B.X, original.B.Y, 1 },
				{ original.C.X, original.C.Y, 1 }
			});

			GetPointsOfSide(original, side, out Vector2 linePoint1, out Vector2 linePoint2);

			Vector2 direction = Vector2.Direction(linePoint1, linePoint2);
			Vector2 middlePoint = (linePoint1 + linePoint2) / 2.0;
			double rotation = direction.Slope;
			
			Matrix shiftMatrix = affineTransformator.FieldTranslation(middlePoint.X, middlePoint.Y);
			Matrix rotationMatrix = affineTransformator.FieldRotation(rotation);
			Matrix scaleMatrix = affineTransformator.Scaling(scaleParameter, -scaleParameter);
			Matrix rotationBackMatrix = affineTransformator.FieldRotation(-rotation);
			Matrix shiftBackMatrix = affineTransformator.FieldTranslation(-middlePoint.X, -middlePoint.Y);

			Matrix transformationMatrix = shiftMatrix * rotationMatrix * scaleMatrix * rotationBackMatrix * shiftBackMatrix;
			Matrix newTriangleMatrix = triangleMatrix * transformationMatrix;

			Vector2 a = new Vector2(newTriangleMatrix[0, 0], newTriangleMatrix[1, 0]);
			Vector2 b = new Vector2(newTriangleMatrix[0, 1], newTriangleMatrix[1, 1]);
			Vector2 c = new Vector2(newTriangleMatrix[0, 2], newTriangleMatrix[1, 2]);

			return new Triangle(a, b, c);
		}

		void GetPointsOfSide(Triangle triangle, Triangle.Side side, out Vector2 point1, out Vector2 point2)
		{
			switch (side)
			{
				case Triangle.Side.AB:
					point1 = triangle.A;
					point2 = triangle.B;
					break;
				case Triangle.Side.BC:
					point1 = triangle.B;
					point2 = triangle.C;
					break;
				case Triangle.Side.CA:
					point1 = triangle.C;
					point2 = triangle.A;
					break;
				default:
					point1 = new Vector2();
					point2 = new Vector2();
					break;
			}
		}
	}
}