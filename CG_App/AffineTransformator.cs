﻿using System;
using CG_App.Tools;

namespace CG_App
{
	class AffineTransformator
	{
		public Matrix Translation(double dx, double dy) => new Matrix(new double[,]
		{
			{ 1,  0,  dx },
			{ 0,  1,  dy },
			{ 0,  0,  1  }
		});

		public Matrix Scaling(double sx, double sy) => new Matrix(new double[,]
		{
			{ sx, 0,  0 },
			{ 0,  sy, 0 },
			{ 0,  0,  1 }
		});

		public Matrix Rotation(double alpha)
		{
			double cosA = Math.Cos(alpha);
			double sinA = Math.Sin(alpha);

			return new Matrix(new double[,]
			{
				{  cosA,  sinA, 0  },
				{ -sinA,  cosA, 0  },
				{  0,     0,    1  }
			});
		}

		public Matrix FieldTranslation(double dx, double dy) => new Matrix(new double[,]
		{
			{  1,    0,    0  },
			{  0,    1,    0  },
			{ -dx,  -dy,   1  }
		});

		public Matrix FieldScaling(double sx, double sy) => new Matrix(new double[,]
		{
			{ 1/sx, 0,    0 },
			{ 0,    1/sy, 0 },
			{ 0,    0,    1 }
		});

		public Matrix FieldRotation(double alpha)
		{
			double cosA = Math.Cos(alpha);
			double sinA = Math.Sin(alpha);

			return new Matrix(new double[,]
			{
				{ cosA, -sinA,  0 },
				{ sinA,  cosA,  0 },
				{ 0,     0,     1 }
			});
		}
	}
}
