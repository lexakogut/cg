﻿using System;

namespace CG_App
{
	public struct Vector2
	{
		public double X { get; set; }
		public double Y { get; set; }

		public Vector2(double x, double y)
		{
			X = x;
			Y = y;
		}

		public double SquaredLength => X * X + Y * Y;
		public double Length => Math.Sqrt(SquaredLength);

		public double Slope => Math.Atan2(Y, X);

		public static Vector2 operator + (Vector2 a, Vector2 b)
		{
			return new Vector2(a.X + b.X, a.Y + b.Y);
		}

		public static Vector2 operator - (Vector2 a, Vector2 b)
		{
			return new Vector2(a.X - b.X, a.Y - b.Y);
		}

		public static Vector2 operator * (Vector2 a, double multiplier)
		{
			return new Vector2(a.X * multiplier, a.Y * multiplier);
		}

		public static Vector2 operator * (double multiplier, Vector2 a)
		{
			return a * multiplier;
		}

		public static Vector2 operator / (Vector2 a, double multiplier)
		{
			return new Vector2(a.X / multiplier, a.Y / multiplier);
		}

		public static Vector2 operator / (double multiplier, Vector2 a)
		{
			return new Vector2(multiplier / a.X, multiplier / a.Y);
		}

		public static Vector2 Direction(Vector2 from, Vector2 to)
		{
			return to - from;
		}
		
		public static double Distance(Vector2 from, Vector2 to)
		{
			return Direction(from, to).Length;
		}

		public static Vector2 Polar(double angle, double h)
		{
			return new Vector2(Math.Cos(angle) * h, Math.Sin(angle) * h);
		}
	}
}
