﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace CG_App
{
    class MyBitmap
    {
		private Color[,] pixels;

		public MyBitmap(int width, int height)
		{
			this.pixels = new Color[height, width];
		}

		public MyBitmap(Color[,] pixels)
		{
			this.pixels = (Color[,])pixels.Clone();
		}

		public int Width => pixels.GetLength(1);
		public int Height => pixels.GetLength(0);

		public bool IsInBounds(int x, int y)
		{
			return (x >= 0) && (x < Width) && (y >= 0) && (y < Height);
		}

		private void ThrowIfNotInBounds(int x, int y)
		{
			if (!IsInBounds(x, y))
			{
				throw new ArgumentException("Arguments are out of bounds");
			}
		}

		public Color GetPixel(int x, int y)
		{
			ThrowIfNotInBounds(x, y);
			return pixels[y, x];
		}

		public void SetPixel(int x, int y, Color newColor)
		{
			ThrowIfNotInBounds(x, y);
			pixels[y, x] = newColor;
		}

		public Color this[int x, int y]
		{
			get => GetPixel(x, y);
			set => SetPixel(x, y, value);
		}

		private void SetColorAtBGRArray(byte[] pixelsBGR, int position, Color color)
		{
			pixelsBGR[position * 4 + 0] = color.B;
			pixelsBGR[position * 4 + 1] = color.G;
			pixelsBGR[position * 4 + 2] = color.R;
			pixelsBGR[position * 4 + 3] = 0;
		}

		public byte[] GenerateBGRArray()
		{
			byte[] pixelsBGR = new byte[Width * Height * 4];
			
			for (int y = 0; y < Height; ++y)
			{
				for (int x = 0; x < Width; ++x)
				{
					int position = y * Width + x;
					Color color = GetPixel(x, y);
					SetColorAtBGRArray(pixelsBGR, position, color);
				}
			}

			return pixelsBGR;
		}

		public Bitmap GenerateBitmap()
		{
			Bitmap bitmap = new Bitmap(Width, Height);

			for (int y = 0; y < Height; ++y)
			{
				for (int x = 0; x < Width; ++x)
				{
					Color pixel = GetPixel(x, y);
					bitmap.SetPixel(x, y, pixel);
				}
			}

			return bitmap;
		}

		public static MyBitmap FromGDIBitmap(Bitmap gdiBitmap)
		{
			MyBitmap bitmap = new MyBitmap(gdiBitmap.Width, gdiBitmap.Height);

			for (int y = 0; y < gdiBitmap.Height; ++y)
			{
				for (int x = 0; x < gdiBitmap.Width; ++x)
				{
					int position = y * gdiBitmap.Width + x;
					Color color = gdiBitmap.GetPixel(x, y);
					bitmap.SetPixel(x, y, color);
				}
			}

			return bitmap;
		}
	}
}
