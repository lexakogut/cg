﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_App
{
	interface ICMYKEffect
	{
		ColorCMYK ApplyEffect(ColorCMYK cmyk);
	}

	class CMYKNegativeEffect : ICMYKEffect
	{
		public ColorCMYK ApplyEffect(ColorCMYK cmyk)
		{
			float c = cmyk.Cyan;
			float m = cmyk.Magenta;
			float y = cmyk.Yellow;
			float k = cmyk.Black;

			return new ColorCMYK(1 - c, 1 - m, 1 - y, 1 - k);
		}
	}

	class CMYKCyanEditEffect : ICMYKEffect
	{
		public float NewCyan { get; set; }

		public ColorCMYK ApplyEffect(ColorCMYK cmyk)
		{
			ColorCMYK cmykResult = cmyk;
			cmykResult.Cyan = NewCyan;
			return cmykResult;
		}
	}
}
