﻿using System;

namespace CG_App.Tools
{
	public class Matrix
	{
		private double[,] values;

		public int Width => values.GetLength(1);
		public int Height => values.GetLength(0);

		public double this[int x, int y]
		{
			get => values[y, x];
			set
			{
				values[y, x] = value;
			}
		}

		public Matrix(int width, int height)
		{
			this.values = new double[height, width];
		}

		public Matrix(double[,] values)
		{
			this.values = (double[,])values.Clone();
		}

		private static void CheckSizes(Matrix a, Matrix b)
		{
			if (a.Width != b.Width || a.Height != b.Height)
			{
				throw new ArgumentException("Matrix sizes are not equal.");
			}
		}

		private static Matrix Iterate2Matrices(Matrix a, Matrix b, Func<double, double, double> iterationFunction)
		{
			Matrix result = new Matrix(a.Width, a.Height);

			for (int y = 0; y < result.Height; ++y)
			{
				for (int x = 0; x < result.Width; ++x)
				{
					double aValue = a[x, y];
					double bValue = b[x, y];
					result[x, y] = iterationFunction(aValue, bValue);
				}
			}

			return result;
		}

		private static Matrix IterateMatrix(Matrix a, Func<double, double> iterationFunction)
		{
			Matrix result = new Matrix(a.Width, a.Height);

			for (int y = 0; y < result.Height; ++y)
			{
				for (int x = 0; x < result.Width; ++x)
				{
					double aValue = a[x, y];
					result[x, y] = iterationFunction(aValue);
				}
			}

			return result;
		}

		public static Matrix operator +(Matrix a, Matrix b)
		{
			CheckSizes(a, b);
			return Iterate2Matrices(a, b, (aValue, bValue) =>
			{
				return aValue + bValue;
			});
		}

		public static Matrix operator -(Matrix a, Matrix b)
		{
			CheckSizes(a, b);
			return Iterate2Matrices(a, b, (aValue, bValue) =>
			{
				return aValue - bValue;
			});
		}

		public static Matrix operator * (Matrix a, double multiplier)
		{
			return IterateMatrix(a, (aValue) =>
			{
				return aValue * multiplier;
			});
		}

		public static Matrix operator * (double multiplier, Matrix a)
		{
			return a * multiplier;
		}

		public static Matrix operator -(Matrix a)
		{
			return IterateMatrix(a, (aValue) =>
			{
				return -aValue;
			});
		}

		public static Matrix operator /(Matrix a, double divisor)
		{
			return IterateMatrix(a, (aValue) =>
			{
				return aValue / divisor;
			});
		}

		public static Matrix operator /(double divident, Matrix b)
		{
			return IterateMatrix(b, (bValue) =>
			{
				return divident / bValue;
			});
		}

		public static Matrix Multiply(Matrix a, Matrix b)
		{
			if (a.Width != b.Height)
			{
				throw new ArgumentException("A width != B height");
			}

			int termCount = a.Width;
			Matrix result = new Matrix(b.Width, a.Height);

			for (int y = 0; y < a.Height; ++y)
			{
				for (int x = 0; x < b.Width; ++x)
				{
					double sum = 0;
					for (int i = 0; i < termCount; ++i)
					{
						double aValue = a[i, y];
						double bValue = b[x, i];
						sum += aValue * bValue;
					}
					result[x, y] = sum;
				}
			}

			return result;
		}

		public static Matrix operator *(Matrix a, Matrix b)
		{
			return Multiply(a, b);
		}

		public static Matrix MemberwiseMultiply(Matrix a, Matrix b)
		{
			CheckSizes(a, b);
			return Iterate2Matrices(a, b, (aValue, bValue) =>
			{
				return aValue * bValue;
			});
		}

		public static Matrix MemberwiseDivide(Matrix a, Matrix b)
		{
			CheckSizes(a, b);
			return Iterate2Matrices(a, b, (aValue, bValue) =>
			{
				return aValue / bValue;
			});
		}

		public static Matrix Transponate(Matrix origin)
		{
			Matrix result = new Matrix(origin.Height, origin.Width);

			for (int y = 0; y < result.Height; ++y)
			{
				for (int x = 0; x < result.Width; ++x)
				{
					result[x, y] = origin[y, x];
				}
			}

			return result;
		}
	}
}
